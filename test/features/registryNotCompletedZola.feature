
# Feature: Check data verifiers

# Background: Open site and click sign up
# 	Given User navigates to qa.zola.com
# 	And User clicks on Log In button
# 	And User with no registry logs in
# 	And User goes to registry page

# Scenario: Check Next button unavailability on first entry
# 	When User clicks on Next button
# 	Then Next button must not be clickable

# Scenario: Check Next button unavailability with no roles selected
# 	Given User completes all names
# 	When User clicks on Next button
# 	Then Next button must not be clickable

# Scenario: Check names are required 
# 	Given User completes all names
# 	When User deletes all names
# 	Then Text "Required" must be shown bellow everey name field

# Scenario: Check Next button unavailability without names
# 	Given User selects both roles
# 	When User clicks on Next button
# 	Then Next button must not be clickable

# Scenario: Check Next button unavailability with only first names
# 	Given User completes both first names
# 	And User selects both roles
# 	When User clicks on Next button
# 	Then Next button must not be clickable

# Scenario: Check Next button unavailability with only last names
# 	Given User completes both last names
# 	And User selects both roles
# 	When User clicks on Next button
# 	Then Next button must not be clickable

# Scenario: Check I just want to shop feature
# 	When User clicks on I just want to shop
# 	Then Shop page must be shown

# Scenario: Check required date 
#     Given User completes all names
#     And User selects both roles
#     And User clicks on Next button
#     When User clicks on Finish button
#     Then Text "Required" must be shown bellow date field

# Scenario Outline: Scenario Outline name: Check date format
# 	Given User completes all names
# 	And User selects both roles
# 	And User clicks on Next button
# 	And User inputs an invalid date <InvalidDate>
# 	When User clicks on Finish button
# 	Then Text "Invalid date" must be shown bellow date field
#     Examples:
#     | InvalidDate |
#     | fecha  |
#     | 01012013  |
#     | 30/12/2022  |
#     | 05/51/2022  |
#     | 06/06/22 |

# Scenario Outline: Create registry with not allowed dates
#     Given User completes all names
#     And User selects both roles
#     And User clicks on Next button
#     And User inputs a date between <from> and <since> years
#     When User clicks on Finish button
#     Then Text <error> must be shown bellow date field
#     Examples:
#     | from | since | error |
#     | -15  | -17  | "Date cannot be before 2012" |
#     | 11  | 16  | "Date cannot be after 2028"  |






# # Scenario: Create registry with date between current day and 2012
# # 	Given User completes all names
# # 	And User selects both roles
# #     And User clicks on Next button
# #     And User inputs a date between current day and 2012
# #     When User clicks on Finish button
# # 	Then Text "Invalid date" must be shown bellow date field
