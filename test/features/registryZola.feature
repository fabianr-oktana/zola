Feature: Create a wedding registry

Background: Open site and click sign up
	Given User navigates to qa.zola.com
	And User clicks on Sign Up button
	And User signs up
	And User completes all names
	
Scenario: Create new registry
	Given User selects both roles
	And User clicks on Next button
	And User selects a valid date
	When User clicks on Finish button
	Then Text "READY TO HAVE SOME FUN, " must be shown

Scenario: Create registry with same roles for both persons
	And User selects same roles for both persons
	And User clicks on Next button
	And User selects a valid date
	When User clicks on Finish button
	Then Text "READY TO HAVE SOME FUN, " must be shown

Scenario: Create register with undecided date
	Given User selects both roles
	And User clicks on Next button
	And User checks 'We haven't decided yet.' checkbox
	When User clicks on Finish button
	Then Text "READY TO HAVE SOME FUN, " must be shown

Scenario: Create registry with date between 6 and 10 years from now
    Given User selects both roles
    And User clicks on Next button
    And User inputs a date between 6 and 10 years
    When User clicks on Finish button
    Then Text "READY TO HAVE SOME FUN, " must be shown

	