import Page from './page';
import logger from '../../utilities/common-utilities';

class checkList extends Page {

    /**
    * define elements
    */

    get checkListOnBoard()   { return browser.element('.checklist-onboard'); }
    get registeredText()  {return browser.getText('.welcome-text');  }

    waitForRegistredPageToLoad () {
        if(!this.checkListOnBoard.isVisible()){
          this.checkListOnBoard.waitForVisible(10000);
        }
        logger.info('The text seen on screen is: ' + this.registeredText +"'");
      }


      



}

export default new checkList()
