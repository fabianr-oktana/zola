import Page from './page';
import logger from '../../utilities/common-utilities';

class HomePage extends Page {

    /**
    * define elements
    */

    get homeLogo()   { return browser.element('.top-nav__logo'); }

    /**
     * define or overwrite page methods
     */
    open () {
        super.open('https://qa.zola.com/');
        logger.info("navigating to 'https://qa.zola.com/'")
        this.waitForHomePageToLoad();
    }
    /**
     * your page specific methods
     */

    waitForHomePageToLoad () {
      if(!this.homeLogo.isVisible()){
        this.homeLogo.waitForVisible(5000);
      }
    }

}

export default new HomePage()
