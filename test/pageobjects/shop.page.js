import Page from './page';
import logger from '../../utilities/common-utilities';

class ShopPage extends Page {

    /**
    * define elements
    */

    get shopBanner()   { return browser.element('.shop-banner__text.row'); }


}

export default new ShopPage()

