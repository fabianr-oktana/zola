import Page from './page';
import logger from '../../utilities/common-utilities';

class signUpModal extends Page {

    /**
    * define elements
    */

    get signUpEmailInput()   { return browser.element('#signup-email'); }
    get passwordInput()   { return browser.element('#unified-nav__password'); }
    get signUpForFreeButton()     { return browser.element('.form-group > button'); }
    get modalBody()     { return browser.element('.modal-body'); }

    get logInEmailInput()   { return browser.element('#login-email'); }

    get emailError()   { return browser.getText('#signup-email + span'); }
    get passwordError()   { return browser.getText('#unified-nav__password + span'); }
    get hasError()   { return browser.$('.has-error'); }

    


    waitForElement (elementArg) {
      browser.waitForVisible(elementArg,3000);
      elementArg.waitForVisible(timeout);
    }

    waitForModalToLoad () {
      if(!this.modalBody.isVisible()){
        this.modalBody.waitForVisible(3000);
      }
    }

    randomString (length, type) {
      let random = "";
      let char_list = "";

      switch (type){
        case 0: // AlphaNum
          char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; 
          break;
        case 1: // Alpha
          char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; 
          break;
        case 3: // Num
          char_list = "0123456789"; 
          break;
      }

      for(var i=0; i < length; i++ ) {  
        random += char_list.charAt(Math.floor(Math.random() * char_list.length));
      }
      return random;
    }

    submitSignUp (){
      logger.info("Clicking on 'Sign Up For Free' button");
      this.signUpForFreeButton.click();
    }

    inputNewValidEmail (){
      logger.info("Generating random Email...");
      let randomEmail = this.randomString (8, 0).concat("@oktana.io");
      logger.info("Generated email: " + randomEmail);
      this.signUpEmailInput.setValue(randomEmail);
    }

    inputNewValidPassword () {
      logger.info("Generating random Password...");
      let randomPassword = this.randomString (12, 0)
      logger.info("Generated password: " + randomPassword);
      this.passwordInput.setValue(randomPassword);
    }

    inputUsedEmail() {
      logger.info("Using fabianr@oktana.io");
      this.signUpEmailInput.setValue("fabianr@oktana.io");
    }

    clearEmail () {
      logger.info("Clearing Email input");
      this.signUpEmailInput.clearElement();
    }
    
    clearPassword () {
      logger.info("Clearing Password input");
      this.passwordInput.clearElement();
    }

    inputCustomEmail (email) {
      logger.info("Inputting " + email + " into email field");
      this.signUpEmailInput.setValue(email);
    }

    inputCustomPassword (passLength) {
      let password = this.randomString(passLength, 0);
      logger.info("Inputting a password of " + passLength + " into password field (" + password + ")");
      this.passwordInput.setValue(password);
    }

    logInUserNoRegistry () {
      let email = "donotcreateregistry5@oktana.io";
      let pass = "donotcreateregistry5@oktana.io";
      this.logInEmailInput.setValue(email);
      this.passwordInput.setValue(pass);
      this.submitSignUp();
    }
}

export default new signUpModal()
