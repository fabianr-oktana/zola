import Page from './page';
import logger from '../../utilities/common-utilities';

class topContainer extends Page {

    /**
    * define elements
    */

    get singUpButton()   { return browser.element('.top-nav__btn'); }
    get logInButton()   { return browser.element('.top-nav__link.top-nav__login');  }
    get registryButton()  { return browser.element('.top-nav-link__registry .top-nav__link');  }
    get modalBackgroud() {return browser.element('.modal.in'); }

    waitForButtonToLoad () {
    if(!this.singUpButton.isVisible()){
      this.singUpButton.waitForVisible(5000);
      }
    }

    clickSignUp () {
      logger.info("Clicking on Top Container 'Sign Up' button");
      this.waitForButtonToLoad();
      this.singUpButton.click();
    }

    clickLogIn () {
      logger.info("Clicking on Top Container 'Log In' button");
      this.waitForButtonToLoad();
      this.logInButton.click();
    }

    goToRegistry () {
      this.modalBackgroud.waitForExist(5000, true);
      logger.info("Clicking on Top Container 'Registry' button");
      this.registryButton.click();
    }

    
}

export default new topContainer()
