import Page from './page';
import logger from '../../utilities/common-utilities';
import { stringify } from 'querystring';
var FIRSTNAME;
class weddingRegistry extends Page {

    /**
    * define elements
    */

    get welcomeText()   { return browser.getText('.onboard-step-header.hidden-xs'); }
    get primaryFirstName()  { return browser.element('#primaryFirstName');  }
    get primaryLastName()  { return browser.element('#primaryLastName');  }
    get partnerFirstName()  { return browser.element('#partnerFirstName');  }
    get partnerLastName()  { return browser.element('#partnerLastName');  }
    get primaryRoleBride() { return browser.element('#primaryRoleBride ~ span'); }
    get primaryRoleGroom() { return browser.element('#primaryRoleGroom ~ span'); }
    get partnerRoleBride() { return browser.element('#partnerRoleBride ~ span'); }
    get partnerRoleGroom() { return browser.element('#partnerRoleGroom ~ span'); }
    get nextButton()  { return browser.element('.form-group button');  }
    get dateField() { return browser.element('.form-control');  }
    get finishButton()  { return browser.element('.next-button button');  }
    get registeredElement()  {return browser.element('.welcome-text');  }
    get registeredText()  {return browser.getText('.welcome-text');  }
    get registyHeader()   { return browser.element('.onboard-header'); }
    get bigDayTitle() {return browser.element('.onboard-step-header');  }
    get wantToShopLink()  { return browser.element('.onboard-link-bold');}
    get dateHasError()  { return browser.element('.help-block'); }
    get undecidedDate() { return browser.element('.input-label');  }
    get namesError()  { return browser.elements('.help-block'); }
    
    getRandomInt(min, max) {
      min = parseInt(min);
      max + parseInt(max);
      logger.info("min " + min)
      var random = Math.floor((Math.random() * (max - min + 1) ) + min);
      return random;
  }

    testThis(min, max) {
        min = parseInt(min);
        max + parseInt(max);
        logger.info("min " + min)
        var random = Math.floor((Math.random() * (max - min + 1) ) + min);
        return random;
    }

  
    waitForRegistryPageToLoad () {
      if(!this.registyHeader.isVisible()){
        this.registyHeader.waitForVisible(5000);
      }
      logger.info('The text seen on screen is: ' + this.welcomeText +"'");
    }


    randomName (type) {
      let arr = [];
      switch (type) {
        case "fname":
          arr = ["Gertha", "Cleopatra","Tomiko" ,"Mi" ,"Anita" ,"Emelda" ,"Lakita","Antoinette" ,"Matthew" ,"Annamae" ,"Gladis" ,"Rhett" ,"Tijuana" ,"Floyd" ,"Zachariah" ,"Tamela" ,"Lanelle" ,"Terica" ,"Annabell" ,"Barrett" ,"Lettie" ,"Deane" ,"Mohammed" ,"Cecelia" ,"Audrie" ,"Cris" ,"Humberto" ,"Alesha" ,"Dominic" ,"Rubye" ];
          break;
        case "lname":
          arr = ["Smit","Johnson","Williams","Jones","Brown","Davis","Miller","Wilson","Moore"  ,"Taylor","Anderson","Thomas","Jackson","White","Harris","Martin","Thompson","Garcia","Martinez","Robinson","Clark","Rodriguez","Lewis","Lee","Walker","Hall","Allen","Young","Hernandez","King","Wright","Lopez","Hill","Scott","Green","Adams","Baker","Gonzalez","Nelson","Carter","Mitchell","Perez","Roberts","Turner","Phillips","Campbell","Parker","Evans","Edwards","Collins"] ;
          break;
      }
      logger.info("array length: "+ arr.length);
      return arr[(this.getRandomInt(0,arr.length-1))];
    }
  
    inputPrimaryFistName () {
      var fname = this.randomName("fname");
      this.primaryFirstName.setValue(fname);
      logger.info("Inputting primary first name: " + fname);
      FIRSTNAME = JSON.stringify(fname).toString().toUpperCase().replace(/"/g,"");
    }

    inputPrimaryLastName () {
      let lname = this.randomName("lname");
      logger.info("Inputting primary last name: " + lname);
      this.primaryLastName.setValue(lname);
    }

    inputPartnerFistName () {
      let fname = this.randomName("fname");
      logger.info("Inputting partner first name: " + fname);
      this.partnerFirstName.setValue(fname);
    }

    inputPartnerLastName () {
      let lname = this.randomName("lname");
      logger.info("Inputting partner last name: " + lname);
      this.partnerLastName.setValue(lname);
    }

    fillAllNameFields () {
      this.inputPrimaryFistName();
      this.inputPrimaryLastName();
      this.inputPartnerFistName();
      this.inputPartnerLastName();
    }

    selectBothRoles () {
      this.selectPrimaryRoleGroom();
      this.selectPartnerRoleBride();
    }

    selectPrimaryRoleGroom () {
      this.primaryRoleGroom.waitForVisible(5000);
      logger.info("Selecting primary role: groom...");
      this.primaryRoleGroom.click();
    }

    selectPrimaryRoleBride () {
      this.primaryRoleBride.waitForVisible(5000);
      logger.info("Selecting primary role: bride...");
      this.primaryRoleBride.click();
    }

    selectPartnerRoleGroom () {
      this.partnerRoleGroom.waitForVisible(5000);
      logger.info("Selecting partner role: groom...");
      this.partnerRoleGroom.click();
    }

    selectPartnerRoleBride () {
      this.partnerRoleBride.waitForVisible(5000);
      logger.info("Selecting partner role: bride...");
      this.partnerRoleBride.click();
    }

    clickNext () {
      this.nextButton.waitForVisible(5000);
      logger.info("Clicking next button")
      this.nextButton.click();
    }

    inputValidDate () {
      this.customDate(3);
    }

    inputDate(date) {
      logger.info("Inputting date: " + date + " in date field")
      this.dateField.setValue(date);
      this.bigDayTitle.click();
    }

    clickFinish () {
      logger.info("Clicking finish button")
      this.finishButton.click();
    }

    fillBothFirstNameFields () {
      this.inputPrimaryFistName();
      this.inputPartnerFistName();
    }

    fillBothLastNameFields () {
      this.inputPrimaryLastName();
      this.inputPartnerLastName();
    }

    selectBothSameRoles () {
      let random = this.getRandomInt(0,1); 
      if (random === 0) {
        this.selectPrimaryRoleBride();
        this.selectPartnerRoleBride();
      } else {
        this.selectPrimaryRoleGroom();
        this.selectPartnerRoleGroom();
      }
    }

    customDate (int) {
      let today = new Date();
      let dd = today.getDate();
      let mm = today.getMonth()+1; //January is 0!
      let yyyy = today.getFullYear();
      let date = mm + "/" + dd + "/" + (yyyy+int);
      logger.info(yyyy+int)
      this.inputDate (date);
    }

    clickOnWantToShop () {
      this.wantToShopLink.waitForVisible(5000);
      logger.info("Clicking 'I just want to shop")
      this.wantToShopLink.click();
    }

    inputDateBetween(from,since) {
      this.customDate(this.getRandomInt(from,since));
    }

    clickOnUndecidedDate ()   {
      this.undecidedDate.waitForVisible(5000);
      this.undecidedDate.click();
    }

    verifiyRequiredNames (text) {
      let equals = true;
      let values = this.namesError.getText()
      for (let i in values ) {
        if (values[i] != text){
          equals = false;
        }
      }
      return equals;
    }

    attachFirstName (expectedText) {
      return expectedText +  FIRSTNAME + '?'
    }

    clearAllNames() {
      this.primaryFirstName.clearElement();
      this.primaryLastName.clearElement();
      this.partnerFirstName.clearElement();
      this.partnerLastName.clearElement();
      this.partnerRoleBride.click();
    }

    
}

export default new weddingRegistry()
