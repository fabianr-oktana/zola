import { defineSupportCode } from 'cucumber';
import checkListPage from '../pageobjects/checkList.page';
import weddingRegistryPage from '../pageobjects/weddingRegistry.page';
import logger from '../../utilities/common-utilities';

defineSupportCode(function({ Then }) {


Then(/^Text "([^"]*)" must be shown$/, function(expectedText) {
    checkListPage.waitForRegistredPageToLoad();
    logger.info("Validating if strings are equals");
    // console.log(weddingRegistry.attachFirstName(expectedText));
    // expect(weddingRegistry.registeredText).to.equal(weddingRegistry.attachFirstName(expectedText));
    // weddingRegistry.attachFirstName(expectedText);
    // expect(checkListPage.registeredText).to.contain(expectedText);
    // expect(weddingRegistry.registeredText).to.expectedText);

    expect(weddingRegistryPage.attachFirstName(expectedText)).to.equal(checkListPage.registeredText);
  });

  
  });