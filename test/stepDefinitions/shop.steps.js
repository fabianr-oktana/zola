import { defineSupportCode } from 'cucumber';
import shopPage from '../pageobjects/shop.page';
defineSupportCode(function({ Given }) {


  Given(/^Shop page must be shown$/, function() {
    shopPage.shopBanner.waitForVisible(8000);
    shopPage.shopBanner.isVisible();

  });

});