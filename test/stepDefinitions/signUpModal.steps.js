import { defineSupportCode } from 'cucumber';
import signUpModal from '../pageobjects/signUpModal.page';
import logger from '../../utilities/common-utilities';

defineSupportCode(function({Given, When, Then}) {

    Given(/^User inputs a valid and unregistered email address in email field$/, function() {
        signUpModal.waitForModalToLoad();
        signUpModal.inputNewValidEmail();
    });

    Given(/^User inputs a valid password in password field$/, function() {
        signUpModal.inputNewValidPassword();
    });

    Given(/^User inputs a valid and already registered email address in email field$/, function() {
        signUpModal.inputUsedEmail();
    });

    When(/^User clicks on Sign up for free button$/, function() {
        signUpModal.submitSignUp();
    });

    Then(/^text "([^"]*)" must be shown bellow email field$/, function(expectedText) {
        signUpModal.hasError.waitForVisible(2000);
        logger.info("Validating if strings are equals");
        expect(signUpModal.emailError).to.equal(expectedText);
    });

    Then(/^text "([^"]*)" must be shown bellow password field$/, function(expectedText) {
        
        logger.info("Validating if strings are equals");
        expect(signUpModal.passwordError).to.equal(expectedText);
    });

    Given(/^User leaves email field blank$/, function() {
        signUpModal.clearEmail();
    });
    
    Given(/^User leaves password field blank$/, function() {
        signUpModal.clearPassword();
    });

    Given(/^User inputs "([^"]*)" in the email field$/, function(email) {
        signUpModal.inputCustomEmail(email);
    });

    Given(/^User inputs a password of ([0-9]*) characters$/, function(passLength) {
        signUpModal.inputCustomPassword(passLength);
    });

    Given(/^User signs up$/, function() {
        signUpModal.waitForModalToLoad();
        signUpModal.inputNewValidEmail();
        signUpModal.inputNewValidPassword();
        signUpModal.submitSignUp();
    });

    Given(/^User with no registry logs in$/, function() {
        signUpModal.logInUserNoRegistry();
    });
     
    

});



// defineSupportCode(function({When}) {

//   When(/^User clicks on 'Sign up for free' button$/, function() {
    
//       signUpModal.submitSignUp();


//     });
// });

//   Then(/^User must be signed up$/, function() {
//    pending;

//   });

//   Then(/^User must not be signed up$/, function() {
//    pending;

//   });













