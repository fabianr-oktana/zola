import { defineSupportCode } from 'cucumber';
import topContainer from '../pageobjects/topContainer.page';
defineSupportCode(function({ Given }) {


  Given(/^User clicks on Sign Up button$/, function() {

  	topContainer.clickSignUp();

  });

  Given(/^User clicks on Log In button$/, function() {

  	topContainer.clickLogIn();

  });

  Given(/^User goes to registry page$/, function() {
  	topContainer.goToRegistry();
  });
  
  
});

  
  