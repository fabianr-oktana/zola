import { defineSupportCode } from 'cucumber';
import weddingRegistry from '../pageobjects/weddingRegistry.page';
import logger from '../../utilities/common-utilities';

defineSupportCode(function({ Given, Then, When }) {

  Given(/^text "([^"]*)" must be shown on screen$/, function(expectedText) {
    weddingRegistry.waitForRegistryPageToLoad();
    logger.info("Validating if strings are equals");
    expect(weddingRegistry.welcomeText).to.equal(expectedText);
  });

  Given(/^User completes all names$/, function() {
    weddingRegistry.waitForRegistryPageToLoad();
    weddingRegistry.fillAllNameFields();
  });

  Given(/^User completes both first names$/, function() {
    weddingRegistry.waitForRegistryPageToLoad();
    weddingRegistry.fillBothFirstNameFields();
  });
  
  Given(/^User completes both last names$/, function() {
    weddingRegistry.waitForRegistryPageToLoad();
    weddingRegistry.fillBothLastNameFields();
  });

  Given(/^User selects same roles for both persons$/, function() {
    weddingRegistry.selectBothSameRoles();
  });
  
  Given(/^User selects both roles$/, function() {
    weddingRegistry.selectBothRoles();
  });
  
  Given(/^User clicks on Next button$/, function() {
    weddingRegistry.clickNext();
  });


  Given(/^User selects a valid date$/, function() {
    weddingRegistry.inputValidDate();
  });

  When(/^User clicks on Finish button$/, function() {
    weddingRegistry.clickFinish();
  });

  When(/^User inputs an invalid date ([^"]*)$/, function(date) {
    weddingRegistry.inputDate(date);
  });

  When(/^User inputs a date between ([^"]*) and ([^"]*) years$/, function(from, since) {
    weddingRegistry.inputDateBetween(from,since);
  });

  Then(/^Text "([^"]*)" must be shown bellow date field$/, function(text) {
    weddingRegistry.dateHasError.waitForVisible(2000);
    logger.info("Validating if strings are equals");
    expect(weddingRegistry.dateHasError.getText()).to.equal(text);
  });

  When(/^Next button must not be clickable$/, function() {
    logger.info('Is Next button available? ' + weddingRegistry.nextButton.isEnabled())
    expect(weddingRegistry.nextButton.isEnabled()).to.be.false;
  });
  
  Given(/^User clicks on I just want to shop$/, function() {
    weddingRegistry.clickOnWantToShop();
  });

  Given(/^User checks 'We haven't decided yet.' checkbox$/, function() {
    weddingRegistry.clickOnUndecidedDate();
  });
  
  Then(/^Text "([^"]*)" must be shown bellow everey name field$/, function(text) {
    expect(weddingRegistry.verifiyRequiredNames(text)).to.be.true;
  });

  Then(/^User deletes all names$/, function() {
    weddingRegistry.clearAllNames();
  });
  
});

  